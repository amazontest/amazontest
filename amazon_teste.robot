*** Settings ***

Documentation     Essa suite testa o site da Amazon.com.br 
# as conf abaixo servem para deixar os cenarios de testes independente criar um teste para suite inteira é so trocar Teste por Suite
Resource        amazon_resources.robot
Test Setup      Abrir o Navegador 
Test Teardown   Fechar o Navegador 

*** Test Cases ***

Caso de Teste 01 - Acesso ao menu "Eletronicos"
  [Documentation]    Essa suite testa o site da Amazon.com.br
  ...              e verifica a categoria Computadores e informatica
  [Tags]           menus
    Acessar a home page do site Amazon.com.br
    Entrar no menu "Eletrônicos" 
    Verificar se aparece a frase "Eletrônicos e Tecnologia"
    Verificar se o título da pagina fica "Eletrônicos e Tecnologia | Amazon.com.br"
    Verificar se aparece a categoria "Computadores e Informática"


Caso de teste 02 - Pesquesisa de Produto 
    [Documentation]    Esse teste veriifica a busca de um produto
    [Tags]           busca produtos
      Acessar a home page do site Amazon.com.br  
      Digitar o nome de produto "Xbox Series S" no campo de pesquisa 
      Clicar no botão de pesquisa 
      #Verificar o resultado da pesquisa se esta listando o produto pesquisado
