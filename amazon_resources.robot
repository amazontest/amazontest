*** Settings ***

Library    SeleniumLibrary

*** Variables ***

${URL}    http://www.amazon.com.br
${MENU ELETRONICOS}    //a[@href='/Eletronicos-e-Tecnologia/b/?ie=UTF8&node=16209062011&ref_=nav_cs_electronics'][contains(.,'Eletrônicos')]
${HEADER_ELETRONICOS}    //span[@class='a-size-base a-color-base apb-browse-refinements-indent-1 a-text-bold'][contains(.,'Eletrônicos e Tecnologia')]
${TEXTO_HEADER_ELETRONICOS}    Eletrônicos e Tecnologia 
${TITULO}                     Eletrônicos e Tecnologia | Amazon.com.br
${CATEGORIA}                  //span[@dir='auto'][contains(.,'Computadores e Informática')]
${PESQUISA}    twotabsearchtextbox
                 





*** Keywords ***

Abrir o Navegador
    Open Browser    browser=chrome
    Maximize Browser Window

Fechar o Navegador
    Capture Page Screenshot
    Sleep    5s    
    Close Browser
    


Acessar a home page do site Amazon.com.br
    Go To    url=${URL}
    Wait Until Element Is Visible    locator=${MENU ELETRONICOS} 

Entrar no menu "Eletrônicos"
    Click Element    locator=${MENU ELETRONICOS}

Verificar se aparece a frase "Eletrônicos e Tecnologia"

    Wait Until Page Contains    text=${TEXTO_HEADER_ELETRONICOS}

    Wait Until Element Is Visible    locator=${HEADER_ELETRONICOS}

Verificar se o título da pagina fica "Eletrônicos e Tecnologia | Amazon.com.br"
    Title Should Be    title=${TITULO}

Verificar se aparece a categoria "Computadores e Informática"      
    Click Element   locator=${CATEGORIA}

Digitar o nome de produto "${PRODUTO}" no campo de pesquisa 
    Input Text    locator=${PESQUISA}   text=${PRODUTO}
    
      
Clicar no botão de pesquisa

    Click Element    locator=nav-search-submit-button 
     




    


     


